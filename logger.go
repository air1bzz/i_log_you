package ilogyou

import (
	"fmt"
	"os"

	"github.com/rs/zerolog"
)

// Logger is the type embedding a logger
type Logger struct {
	logger zerolog.Logger
}

// GetLogger returns a basic logger.
func GetLogger() *Logger {
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	zerolog.CallerSkipFrameCount = 3
	output := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: "06-01-02 15:04:05"}

	logger := zerolog.New(output).With().Timestamp().Logger()
	return &Logger{logger}
}

// Mute disables logger.
func (l *Logger) Mute() {
	zerolog.SetGlobalLevel(zerolog.Disabled)
}

// SetLevel sets level of logger, if level doesn't exist.
func (l *Logger) SetLevel(level string) {
	lvl, err := zerolog.ParseLevel(level)
	if err != nil {
		panic(fmt.Sprintf("Level not found: %v", err))
	}

	zerolog.SetGlobalLevel(lvl)
}

// Debugf logs formated message with args at debug level.
func (l *Logger) Debugf(format string, a ...interface{}) { l.logger.Debug().Caller().Msgf(format, a...) }

// Debug logs an message at debug level.
func (l *Logger) Debug(a ...interface{}) { l.logger.Debug().Caller().Msg(fmt.Sprint(a...)) }

// Infof logs formated message with args at info level.
func (l *Logger) Infof(format string, a ...interface{}) { l.logger.Info().Msgf(format, a...) }

// Info logs message at info level.
func (l *Logger) Info(a ...interface{}) { l.logger.Info().Msg(fmt.Sprint(a...)) }

// Warnf logs formated message with args at warning level.
func (l *Logger) Warnf(format string, a ...interface{}) { l.logger.Warn().Msgf(format, a...) }

// Warn logs message at warning level.
func (l *Logger) Warn(a ...interface{}) { l.logger.Warn().Msg(fmt.Sprint(a...)) }

// Errorf logs formated message with args at error level.
func (l *Logger) Errorf(format string, a ...interface{}) {
	l.logger.Error().Caller().Msgf(fmt.Sprintf(format, a...))
}

// Error logs message at error level.
func (l *Logger) Error(a ...interface{}) { l.logger.Error().Caller().Msg(fmt.Sprint(a...)) }

// Panicf logs formated message with args at panic level.
func (l *Logger) Panicf(format string, a ...interface{}) { l.logger.Panic().Caller().Msgf(format, a...) }

// Panic logs message at panic level.
func (l *Logger) Panic(a ...interface{}) { l.logger.Panic().Caller().Msg(fmt.Sprint(a...)) }
